package com.example.TableFootball.DAO.Impl;

import com.example.TableFootball.DAO.PlayerDAO;
import com.example.TableFootball.DTO.request.PlayerRequestDTO;
import com.example.TableFootball.DTO.response.IdAndLoginResponseDTO;
import com.example.TableFootball.DTO.response.PlayerResponseDTO;
import com.example.TableFootball.exception.PlayerNotFoundById;
import com.example.TableFootball.models.Player;
import com.example.TableFootball.models.Role;
import lombok.RequiredArgsConstructor;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Qualifier("PlayerEntityManagerDAO")
@Primary
@RequiredArgsConstructor
public class PlayerEntityManagerDAOImpl implements PlayerDAO {

    @PersistenceContext
    private EntityManager entityManager;

    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;

    //-------------EntityManager-------------
    @Override
    @Transactional
    public PlayerResponseDTO createPlayer(PlayerRequestDTO requestDTO) {
        requestDTO.setPassword(passwordEncoder.encode(requestDTO.getPassword()));

        Player player = modelMapper.map(requestDTO, Player.class);
        player.setRole(Role.PLAYER);
        entityManager.persist(player);

        return modelMapper.map(player, PlayerResponseDTO.class);
    }

    @Override
    public List<PlayerResponseDTO> getAllPlayers() {
        List<Player> players = entityManager.createQuery("select p from Player p", Player.class).getResultList();

        return players
                .stream()
                .map(player -> modelMapper.map(player, PlayerResponseDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public PlayerResponseDTO getPlayerById(Long id) {
        Player player = getPlayerEntityById(id, "Ирок не найден по id - ");

        return modelMapper.map(player, PlayerResponseDTO.class);
    }

    @Override
    public Player getPlayerByLogin(String login) {
        return Optional
                .ofNullable(entityManager
                .createQuery("select p from Player p where login = ?1", Player.class)
                .setParameter(1, login)
                .getSingleResult())
                .orElseThrow(() -> new UsernameNotFoundException("User doesn't exists"));
    }

    @Override
    public Long getIdByLogin(String login) {
        return Optional
                .ofNullable(entityManager
                        .createQuery("select p from Player p where login = ?1", Player.class)
                        .setParameter(1, login)
                        .getSingleResult())
                .orElseThrow(() -> new UsernameNotFoundException("User doesn't exists")).getId();
    }

    @Override
    public String getRoleByLogin(String login) {
        return Optional
                .ofNullable(entityManager
                        .createQuery("select p from Player p where login = ?1", Player.class)
                        .setParameter(1, login)
                        .getSingleResult())
                .orElseThrow(() -> new UsernameNotFoundException("User doesn't exists")).getRole().toString();
    }

    @Override
    @Transactional
    public void deletePlayer(Long id) {
        entityManager.createQuery("delete from Player p where p.id=:id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    @Transactional
    public IdAndLoginResponseDTO updatePlayerById(Long id, PlayerRequestDTO requestDTO) {
        Player player = Optional
                .ofNullable(entityManager.find(Player.class, id))
                .orElseThrow(() -> new PlayerNotFoundById("Player not found by id - " + id));

        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        modelMapper.map(requestDTO, player);

        entityManager.persist(player);

        return modelMapper.map(player, IdAndLoginResponseDTO.class);
    }

    @Override
    public void setNewRole(Long id, Role role) {
        Player player = getPlayerEntityById(id, "Ирок не найден по id - ");
        player.setRole(role);

        entityManager.persist(player);
    }

    @Override
    public void checkPlayerById(Long id, String msg) {
        getPlayerEntityById(id, msg);
    }

    private Player getPlayerEntityById (Long id, String msg) {
        return Optional
                .ofNullable(entityManager.find(Player.class, id))
                .orElseThrow(() -> new PlayerNotFoundById(msg + id));
    }
}
