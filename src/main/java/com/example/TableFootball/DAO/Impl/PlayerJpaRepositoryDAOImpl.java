package com.example.TableFootball.DAO.Impl;

import com.example.TableFootball.DAO.PlayerDAO;
import com.example.TableFootball.DTO.request.PlayerRequestDTO;
import com.example.TableFootball.DTO.response.IdAndLoginResponseDTO;
import com.example.TableFootball.DTO.response.PlayerResponseDTO;
import com.example.TableFootball.exception.PlayerNotFoundById;
import com.example.TableFootball.models.Player;
import com.example.TableFootball.models.Role;
import com.example.TableFootball.repository.PlayerRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Qualifier("PlayerJpaRepositoryDAO")
@RequiredArgsConstructor
public class PlayerJpaRepositoryDAOImpl implements PlayerDAO {

    private final PlayerRepository playerRepository;

    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;

    //-------------JpaRepository-------------
    @Override
    public PlayerResponseDTO createPlayer(PlayerRequestDTO requestDTO) {
        requestDTO.setPassword(passwordEncoder.encode(requestDTO.getPassword()));

        Player player = modelMapper.map(requestDTO, Player.class);
        player.setRole(Role.PLAYER);

        playerRepository.saveAndFlush(player);

        return modelMapper.map(player, PlayerResponseDTO.class);
    }

    @Override
    public List<PlayerResponseDTO> getAllPlayers() {
        return playerRepository
                .findAll()
                .stream()
                .map(player -> modelMapper.map(player, PlayerResponseDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public PlayerResponseDTO getPlayerById(Long id) {
        Player player = playerRepository
                .findById(id)
                .orElseThrow(() -> new PlayerNotFoundById("Player not found by id - " + id));

        return modelMapper.map(player, PlayerResponseDTO.class);
    }

    @Override
    public Player getPlayerByLogin(String login) {
        return playerRepository
                .findByLogin(login)
                .orElseThrow(() -> new UsernameNotFoundException("User doesn't exists"));
    }

    @Override
    public Long getIdByLogin(String login) {
        return playerRepository
                .findByLogin(login)
                .orElseThrow(() -> new UsernameNotFoundException("User doesn't exists")).getId();
    }

    @Override
    public String getRoleByLogin(String login) {
        return playerRepository
                .findByLogin(login)
                .orElseThrow(() -> new UsernameNotFoundException("User doesn't exists")).getRole().toString();
    }

    @Override
    public void deletePlayer(Long id) {
        playerRepository.deleteById(id);
    }

    @Override
    public IdAndLoginResponseDTO updatePlayerById(Long id, PlayerRequestDTO requestDTO) {
        Player player = playerRepository
                .findById(id)
                .orElseThrow(() -> new PlayerNotFoundById("Player not found by id - " + id));

        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        modelMapper.map(requestDTO, player);

        playerRepository.saveAndFlush(player);
        return modelMapper.map(player, IdAndLoginResponseDTO.class);
    }

    @Override
    public void setNewRole(Long id, Role role) {
        Player player = getPlayerEntityById(id, "Не существует игрока с id - ");
        player.setRole(role);

        playerRepository.saveAndFlush(player);
    }

    @Override
    public void checkPlayerById(Long id, String msg) {
        getPlayerEntityById(id, msg);
    }

    private Player getPlayerEntityById(Long id, String msg) {
        return playerRepository
                .findById(id)
                .orElseThrow(() -> new UsernameNotFoundException("Не существует игрока с id - " + id));
    }
}
