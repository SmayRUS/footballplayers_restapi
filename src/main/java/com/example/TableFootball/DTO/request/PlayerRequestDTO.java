package com.example.TableFootball.DTO.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlayerRequestDTO {
    private String login;

    private String password;
}
