package com.example.TableFootball.DTO.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IdAndLoginResponseDTO {
    Long id;

    String login;
}
