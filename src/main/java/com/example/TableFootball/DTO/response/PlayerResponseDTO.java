package com.example.TableFootball.DTO.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlayerResponseDTO {
    Long id;

    Long ranks;

    Long wins;

    Long loses;

    Long num_matches;

    Double win_rate;
}
