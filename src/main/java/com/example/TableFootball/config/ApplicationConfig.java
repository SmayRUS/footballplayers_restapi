package com.example.TableFootball.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

//    @Bean
//    public SlackClient slackClient() {
//        SlackClientRuntimeConfig runtimeConfig = SlackClientRuntimeConfig.builder()
//                .setTokenSupplier(() -> "xoxb-2591847079812-2587154670325-ok0kg9DeViNigc7IZWdvYsza")
//                .build();
//
//        return SlackClientFactory.defaultFactory().build(runtimeConfig);
//    }
}
