package com.example.TableFootball.controllers;

import com.example.TableFootball.DTO.request.PlayerRequestDTO;
import com.example.TableFootball.service.PlayerService;
import com.example.TableFootball.service.SecurityService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/player/auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final PlayerService playerService;
    private final SecurityService securityService;

    @PostMapping("/login")
    public ResponseEntity<?> authenticate(@RequestBody PlayerRequestDTO requestDTO) {
        try {
            securityService.callAuthenticationManager(requestDTO.getLogin(), requestDTO.getPassword());

            String token = securityService.createToken(requestDTO.getLogin(),
                    playerService.getRoleByLogin(requestDTO.getLogin()));

            Map<Object, Object> response = new HashMap<>();
            response.put("login", requestDTO.getLogin());
            response.put("token", token);
            return ResponseEntity.ok(response);
        } catch (AuthenticationException e) {
            return new ResponseEntity<>("Invalid email/password combination", HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        SecurityContextLogoutHandler securityContextLogoutHandler = new SecurityContextLogoutHandler();
        securityContextLogoutHandler.logout(request, response, null);
    }
}
