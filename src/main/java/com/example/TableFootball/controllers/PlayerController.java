package com.example.TableFootball.controllers;

import com.example.TableFootball.DTO.request.PlayerRequestDTO;
import com.example.TableFootball.DTO.response.IdAndLoginResponseDTO;
import com.example.TableFootball.DTO.response.PlayerResponseDTO;
import com.example.TableFootball.exception.NoChangeSomeoneElsePlayer;
import com.example.TableFootball.exception.PlayerNotFoundById;
import com.example.TableFootball.security.SecurityPlayer;
import com.example.TableFootball.service.PlayerService;
import com.example.TableFootball.service.SlackNotificationService;
import com.slack.api.Slack;
import com.slack.api.methods.MethodsClient;
import com.slack.api.methods.request.chat.ChatPostMessageRequest;
import com.slack.api.methods.response.chat.ChatPostMessageResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/player")
@RequiredArgsConstructor
public class PlayerController {

    private final PlayerService playerService;
    private final SlackNotificationService notificationService;

    @PostMapping("/registration")
    public ResponseEntity<PlayerResponseDTO> regPlayer(@RequestBody PlayerRequestDTO requestDTO) {
        try {
            PlayerResponseDTO responseDTO = playerService.createPlayer(requestDTO);
//----
//            Slack slack = Slack.getInstance();
//----
//            String token = System.getenv("xoxb-2591847079812-2587154670325-ok0kg9DeViNigc7IZWdvYsza");

//            MethodsClient methods = slack.methods("xoxb-2591847079812-2587154670325-ok0kg9DeViNigc7IZWdvYsza");
//
//            ChatPostMessageRequest request = ChatPostMessageRequest.builder()
//                    .channel("#general")
//                    .text("COOOOOL")
//                    .build();


//---
//            ChatPostMessageResponse response = slack
//                    .methods("xoxb-2591847079812-2587154670325-ok0kg9DeViNigc7IZWdvYsza")
//                    .chatPostMessage(req -> req
//                            .channel("#general")
//                            .text("COOL2"));
//----
//            System.out.println(response);
//            notificationService.createNotification(responseDTO);

            return ResponseEntity.ok(responseDTO);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ResponseEntity<List<PlayerResponseDTO>> getAllPlayers() {
        try {
            List<PlayerResponseDTO> responseDTO = playerService.getAllPlayers();

            return new ResponseEntity<List<PlayerResponseDTO>>(responseDTO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('players:read')" +
            "or hasAuthority('creators:read')" +
            "or hasAuthority('opponents:read')")
    public ResponseEntity responsePlayerId(@PathVariable(value = "id") Long id) {
        try {
            PlayerResponseDTO responseDTO = playerService.getPlayerById(id);

            return ResponseEntity.ok(responseDTO);
        } catch (PlayerNotFoundById e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize(value = "@securityServiceImpl.checkAccessPlayerIdForChange(#securityPlayer.getId, #id)")
    public ResponseEntity deletePlayerId(@PathVariable(value = "id") Long id, @AuthenticationPrincipal SecurityPlayer securityPlayer) {
        try {
            playerService.deletePlayer(id);

//            notificationService.deleteNotification(responseDTO);

            return ResponseEntity.ok("Delete complete!");
//        } catch (NoChangeSomeoneElsePlayer e) {
//            return new ResponseEntity<>(e.getMessage(), HttpStatus.FORBIDDEN);
        } catch (PlayerNotFoundById e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize(value = "@securityServiceImpl.checkAccessPlayerIdForChange(#securityPlayer, #id)")
    public ResponseEntity updatePlayerId(@PathVariable(value = "id") Long id, @RequestBody PlayerRequestDTO requestDTO, @AuthenticationPrincipal SecurityPlayer securityPlayer) {
        try {
            IdAndLoginResponseDTO responseDTO = playerService.updatePlayerById(id, requestDTO);

//            notificationService.updateNotification(responseDTO);

            return ResponseEntity.ok(responseDTO);
        } catch (PlayerNotFoundById e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
//        } catch (NoChangeSomeoneElsePlayer e) {
//            return new ResponseEntity<>(e.getMessage(), HttpStatus.FORBIDDEN);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
