package com.example.TableFootball.exception;

public class NoChangeSomeoneElsePlayer extends Exception {
    public NoChangeSomeoneElsePlayer(String message) {
        super(message);
    }
}
