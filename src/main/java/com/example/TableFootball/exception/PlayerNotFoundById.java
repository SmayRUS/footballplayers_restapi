package com.example.TableFootball.exception;

public class PlayerNotFoundById extends RuntimeException {
    public PlayerNotFoundById(String message) {
        super(message);
    }
}
