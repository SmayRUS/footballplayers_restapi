package com.example.TableFootball.models;

public enum Permission {
    PLAYERS_WRITE("players:write"),
    PLAYERS_READ("players:read"),
    CREATORS_WRITE("creators:write"),
    CREATORS_READ("creators:read"),
    OPPONENTS_WRITE("opponents:write"),
    OPPONENTS_READ("opponents:read");


    private final String permission;

    Permission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}
