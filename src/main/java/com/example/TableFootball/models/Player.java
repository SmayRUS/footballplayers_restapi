package com.example.TableFootball.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String login;

    private String password;

    private Long ranks;

    private Long wins;

    private Long loses;

    private Long num_matches;

    private Double win_rate;

    @Enumerated(value = EnumType.STRING)
    private Role role;
}
