package com.example.TableFootball.models;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

public enum Role {
    PLAYER(Set.of(Permission.PLAYERS_WRITE, Permission.PLAYERS_READ)),
    CREATOR(Set.of(Permission.CREATORS_WRITE, Permission.CREATORS_READ)),
    OPPONENT(Set.of(Permission.OPPONENTS_WRITE, Permission.OPPONENTS_READ));

    private final Set<Permission> permissions;

    Role(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public Set<SimpleGrantedAuthority> getAuthorities() {
        return getPermissions().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toSet());
    }
}
