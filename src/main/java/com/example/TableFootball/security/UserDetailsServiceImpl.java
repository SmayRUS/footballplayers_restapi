package com.example.TableFootball.security;

import com.example.TableFootball.models.Player;
import com.example.TableFootball.repository.PlayerRepository;
import com.example.TableFootball.service.PlayerService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;

@Service("userDetailsServiceImpl")
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    @PersistenceContext
    private final EntityManager entityManager;


    @Override
    public SecurityPlayer loadUserByUsername(String login) throws UsernameNotFoundException {
        Player player = Optional
                .ofNullable(entityManager
                        .createQuery("select p from Player p where login = ?1", Player.class)
                        .setParameter(1, login)
                        .getSingleResult())
                .orElseThrow(() -> new UsernameNotFoundException("User doesn't exists"));

        return new SecurityPlayer(player);
    }
}
