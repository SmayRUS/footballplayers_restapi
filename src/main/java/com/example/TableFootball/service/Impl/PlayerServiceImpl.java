package com.example.TableFootball.service.Impl;

import com.example.TableFootball.DAO.PlayerDAO;
import com.example.TableFootball.DTO.request.PlayerRequestDTO;
import com.example.TableFootball.DTO.response.IdAndLoginResponseDTO;
import com.example.TableFootball.DTO.response.PlayerResponseDTO;
import com.example.TableFootball.models.Player;
import com.example.TableFootball.models.Role;
import com.example.TableFootball.service.PlayerService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PlayerServiceImpl implements PlayerService {

    @Autowired
    @Qualifier("PlayerEntityManagerDAO")
//    @Qualifier("PlayerJpaRepositoryDAO")
    private final PlayerDAO playerDAO;

    @Override
    public PlayerResponseDTO createPlayer(PlayerRequestDTO requestDTO) {
        return playerDAO.createPlayer(requestDTO);
    }

    @Override
    public List<PlayerResponseDTO> getAllPlayers() {
        return playerDAO.getAllPlayers();
    }

    @Override
    public PlayerResponseDTO getPlayerById(Long id) {
        return playerDAO.getPlayerById(id);
    }

    @Override
    public Player getPlayerByLogin(String login) {
        return playerDAO.getPlayerByLogin(login);
    }

    @Override
    public Long getIdByLogin(String login) {
        return playerDAO.getIdByLogin(login);
    }

    @Override
    public String getRoleByLogin(String login) {
        return playerDAO.getRoleByLogin(login);
    }

    @Override
    public void deletePlayer(Long id) {
        playerDAO.deletePlayer(id);
    }

    @Override
    public IdAndLoginResponseDTO updatePlayerById(Long id, PlayerRequestDTO requestDTO) {
        return playerDAO.updatePlayerById(id, requestDTO);
    }

    @Override
    public void setNewRole(Long id, Role role) {
        playerDAO.setNewRole(id, role);
    }

    @Override
    public void checkPlayerById(Long id, String msg) {
        playerDAO.checkPlayerById(id, msg);
    }
}
