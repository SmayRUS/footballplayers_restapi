package com.example.TableFootball.service.Impl;

import com.example.TableFootball.security.JwtTokenProvider;
import com.example.TableFootball.security.SecurityPlayer;
import com.example.TableFootball.service.GameService;
import com.example.TableFootball.service.SecurityService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SecurityServiceImpl implements SecurityService {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final GameService gameService;

    @Override
    public void callAuthenticationManager(String login, String password) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login, password));
    }

    @Override
    public String createToken(String login, String role) {
        return jwtTokenProvider.createToken(login, role);
    }

    @Override
    public Boolean checkAccessPlayerIdForChange(SecurityPlayer securityPlayer, Long idEqual) {
        return securityPlayer.getId().equals(idEqual);
    }

    @Override
    public Boolean checkAccessCreatorOrOpponentForChange(SecurityPlayer securityPlayer, Long gameId) {
        if (securityPlayer.getId().equals(gameService.getCreatorIdByGameId(gameId)))
            return true;
        else
            return securityPlayer.getId().equals(gameService.getOpponentIdByGameId(gameId));
    }

    @Override
    public Boolean checkAccessCreatorForChange(SecurityPlayer securityPlayer, Long gameId) {
        return securityPlayer.getId().equals(gameService.getCreatorIdByGameId(gameId));
    }

    @Override
    public Boolean checkAccessOpponentForChange(SecurityPlayer securityPlayer, Long gameId) {
        return securityPlayer.getId().equals(gameService.getOpponentIdByGameId(gameId));
    }
}
