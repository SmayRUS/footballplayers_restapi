package com.example.TableFootball.service;

import com.example.TableFootball.DTO.request.PlayerRequestDTO;
import com.example.TableFootball.DTO.response.IdAndLoginResponseDTO;
import com.example.TableFootball.DTO.response.PlayerResponseDTO;
import com.example.TableFootball.models.Player;
import com.example.TableFootball.models.Role;

import java.util.List;

public interface PlayerService {
     PlayerResponseDTO createPlayer(PlayerRequestDTO requestDTO);
     List<PlayerResponseDTO> getAllPlayers();
     PlayerResponseDTO getPlayerById(Long id);
     Player getPlayerByLogin(String login);
     Long getIdByLogin(String login);
     String getRoleByLogin(String login);
     void deletePlayer(Long id);
     IdAndLoginResponseDTO updatePlayerById(Long id, PlayerRequestDTO requestDTO);
     void setNewRole(Long id, Role role);
     void checkPlayerById(Long id, String msg);
}
