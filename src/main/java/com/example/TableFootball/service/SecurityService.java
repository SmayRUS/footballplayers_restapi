package com.example.TableFootball.service;

import com.example.TableFootball.security.SecurityPlayer;

public interface SecurityService {
    void callAuthenticationManager (String login, String password);
    String createToken (String login, String role);
    Boolean checkAccessPlayerIdForChange (SecurityPlayer securityPlayer, Long idEqual);
    Boolean checkAccessCreatorOrOpponentForChange(SecurityPlayer securityPlayer, Long gameId);
    Boolean checkAccessCreatorForChange(SecurityPlayer securityPlayer, Long gameId);
    Boolean checkAccessOpponentForChange(SecurityPlayer securityPlayer, Long gameId);
}
