package com.example.SimpleList;

import com.simplecollections.simplelist.SimpleArrayList;
import com.simplecollections.simplelist.SimpleList;
import com.simplecollections.simplelist.exceptions.IndexOutsideTheSizeOfSimpleList;

import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;

import java.util.*;
import java.util.stream.Stream;

public class SimpleListTest {
    private SimpleList<String> listString;
    private SimpleList<String> addStringList;
    private SimpleList<Integer> listInteger;
    private SimpleList<Integer> compareIntegerList;

    @Before
    public void variableInit() {
        listString = getFullStringSimpleList("test ",5);
        addStringList = getFullStringSimpleList("add ", 3);
        listInteger = getFullIntegerSimpleList(5);
        compareIntegerList = getFullIntegerSimpleList(5);
    }

    @Test
    public void addOneValueToSimpleList() {
        listString.add("test 6");
        Assert.assertEquals(6, listString.size());
        Assert.assertEquals("test 6", listString.get(5).get());
    }

    @Test
    public void insertOneValueToSimpleList() throws IndexOutsideTheSizeOfSimpleList {
        listString.insert(2, "COOL");
        Assert.assertEquals(6, listString.size());
        Assert.assertEquals("COOL", listString.get(2).get());
    }

    @Test
    public void removeOneValueToSimpleList() throws IndexOutsideTheSizeOfSimpleList {
        listString.remove(2);
        Assert.assertEquals(4, listString.size());
        Assert.assertEquals("test 4", listString.get(2).get());
    }

    @Test
    public void getOneValueSimpleList() {
        Assert.assertEquals("test 5", listString.get(4).get());
        Assert.assertEquals(Optional.empty(), listString.get(5));
    }

    @Test
    public void sizeOfAllValuesSimpleList() {
        Assert.assertEquals(5, listString.size());
    }

    @Test
    public void addAllValuesFromSimpleListToSimpleList() {
        listString.addAll(addStringList);

        Assert.assertEquals(5, listString.size());
    }

    @Test
    public void firstEntryValueInSimpleList() {
        SimpleList<String> listString = getFullStringSimpleList("test ",5);

        Assert.assertEquals(4, listString.first("test 5"));
        Assert.assertEquals(-1, listString.first("lololololol"));
    }

    @Test
    public void lastEntryValueInSimpleList() {
        Assert.assertEquals(0, listString.first("test 1"));
        Assert.assertEquals(-1, listString.first("lololololol"));
    }

    @Test
    public void containsValueOrNotInSimpleList() {
        Assert.assertTrue(listString.contains("test 1"));
        Assert.assertFalse(listString.contains("lololololol"));
    }

    @Test
    public void isEmptyOrNotInSimpleList() {
        SimpleList<String> testList = new SimpleArrayList<>();
        Assert.assertTrue(testList.isEmpty());

        testList = getFullStringSimpleList("test ",1);
        Assert.assertFalse(testList.isEmpty());
    }

    @Test
    public void shuffleValuesInSimpleListTest() {
        SimpleList<String> resultList = listString.shuffle();

        Assert.assertNotEquals(resultList, listString);
    }

    @Test
    public void sortValuesInIntegerSimpleList() {
        listInteger = listInteger.shuffle();

        Comparator<Integer> comparator = getIntegerComparatorForSimpleList();

        SimpleList<Integer> resultList = listInteger.sortBubble(comparator);

        Assert.assertNotEquals(resultList, listInteger);
        Assert.assertEquals(compareIntegerList, resultList);
    }

    public SimpleList<String> getFullStringSimpleList(String message, int n) {
        SimpleList<String> resultList = new SimpleArrayList<>();

        Stream.iterate(1, a -> a + 1).limit(n).forEach(a -> resultList.add(message + a));

        return resultList;
    }

    public SimpleList<Integer> getFullIntegerSimpleList(int n) {
        SimpleList<Integer> resultList = new SimpleArrayList<>();

        Stream.iterate(1, a -> a + 1).limit(n).forEach(resultList::add);

        return resultList;
    }

    public Comparator<Integer> getIntegerComparatorForSimpleList() {
        return new Comparator<Integer>() {
            @Override
            public int compare(Integer t, Integer t1) {
                return t.compareTo(t1);
            }
        };
    }
}
